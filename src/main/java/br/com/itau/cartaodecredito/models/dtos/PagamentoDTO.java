package br.com.itau.cartaodecredito.models.dtos;

import br.com.itau.cartaodecredito.models.Cartao;
import br.com.itau.cartaodecredito.models.Pagamento;

public class PagamentoDTO {

    private int id;

    private int cartao_id;

    private String descricao;

    private double valor;

    public PagamentoDTO() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCartao_id() {
        return cartao_id;
    }

    public void setCartao_id(int cartao_id) {
        this.cartao_id = cartao_id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public Pagamento converteParaPagamentos(Cartao cartao){
        Pagamento pagamento = new Pagamento();
        pagamento.setCartao(cartao);
        pagamento.setDescricao(this.descricao);
        pagamento.setValor(this.valor);
        return pagamento;
    }
}
