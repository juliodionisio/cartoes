package br.com.itau.cartaodecredito.models;


import br.com.itau.cartaodecredito.models.dtos.CartaoDTO;
import br.com.itau.cartaodecredito.models.dtos.PagamentoDTO;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
public class Pagamento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne(cascade = CascadeType.ALL)
    @NotNull(message = "É obrigatório informar o ID do cartão")
    private Cartao cartao;

    @NotBlank
    private String descricao;

    @Digits(integer = 10, fraction = 2, message = "O valor está no formato incorreto")
    private double valor;

    public Pagamento() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Cartao getCartao() {
        return cartao;
    }

    public void setCartao(Cartao cartao) {
        this.cartao = cartao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public PagamentoDTO converteParaDTO(){
        PagamentoDTO pagamentoDTO = new PagamentoDTO();
        pagamentoDTO.setId(this.id);
        pagamentoDTO.setCartao_id(this.cartao.getId());
        pagamentoDTO.setDescricao(this.descricao);
        pagamentoDTO.setValor(this.valor);
        return pagamentoDTO;
    }
}
