package br.com.itau.cartaodecredito.models;

import br.com.itau.cartaodecredito.models.dtos.CartaoDTO;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
public class Cartao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotBlank
    private String numero;

    @ManyToOne(cascade = CascadeType.ALL)
    @NotNull(message = "É obrigatório informar o ID do dono do cartão")
    private Cliente cliente;

    private boolean ativo;

    public Cartao() {
        this.ativo = false;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public CartaoDTO converteParaDTO(){
        CartaoDTO cartaoDTO = new CartaoDTO();
        cartaoDTO.setId(this.id);
        cartaoDTO.setIdCliente(this.cliente.getId());
        cartaoDTO.setAtivo(this.ativo);
        cartaoDTO.setNumero(this.numero);
        return cartaoDTO;
    }
}
