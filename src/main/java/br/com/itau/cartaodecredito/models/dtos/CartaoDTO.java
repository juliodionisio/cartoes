package br.com.itau.cartaodecredito.models.dtos;

import br.com.itau.cartaodecredito.models.Cartao;
import br.com.itau.cartaodecredito.models.Cliente;
import br.com.itau.cartaodecredito.services.CartaoService;
import br.com.itau.cartaodecredito.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;

public class CartaoDTO {
    private int id;
    private String numero;
    private int idCliente;
    private boolean ativo;

    @Autowired
    private ClienteService clienteService;

    public CartaoDTO() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public Cartao converterParaCartao(Cliente cliente){
        Cartao cartao = new Cartao();
        cartao.setNumero(this.numero);
        cartao.setCliente(cliente);
        return cartao;
    }
}
