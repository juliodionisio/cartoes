package br.com.itau.cartaodecredito.repositories;

import br.com.itau.cartaodecredito.models.Cartao;
import br.com.itau.cartaodecredito.models.Cliente;
import br.com.itau.cartaodecredito.models.Pagamento;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface PagamentoRepository extends CrudRepository<Pagamento,Integer> {
    List<Pagamento> findAllByCartao(Cartao cartao);
}
