package br.com.itau.cartaodecredito.repositories;

import br.com.itau.cartaodecredito.models.Cartao;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CartaoRepository  extends CrudRepository<Cartao,Integer> {
    Cartao findByNumero(String numero);

    boolean existsByNumero(String numero);
}
