package br.com.itau.cartaodecredito.repositories;

import br.com.itau.cartaodecredito.models.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente,Integer> {
}
