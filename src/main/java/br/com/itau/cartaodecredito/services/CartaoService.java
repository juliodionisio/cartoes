package br.com.itau.cartaodecredito.services;


import br.com.itau.cartaodecredito.models.Cartao;
import br.com.itau.cartaodecredito.repositories.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    CartaoRepository cartaoRepository;

    public Cartao cadastrarCartao(Cartao cartao){
        Cartao novoCartao = cartaoRepository.save(cartao);
        return novoCartao;
    }

    public Cartao buscarCartaoPorNumero(String numero){
        Cartao cartao = cartaoRepository.findByNumero(numero);
        if(cartao != null){
            return cartao;
        } throw new RuntimeException("Cartão não encontrado");
    }

    public Cartao buscarCartaoPorID(int id){
        Optional<Cartao> optionalCartao = cartaoRepository.findById(id);
        if(optionalCartao.isPresent()){
            return optionalCartao.get();
        } throw new RuntimeException("Cartão não encontrado");
    }

    public Cartao ativarCartao(String numero){
        if(cartaoRepository.existsByNumero(numero)){
            buscarCartaoPorNumero(numero).setAtivo(true);
            Cartao cartaoObjeto = cartaoRepository.save(buscarCartaoPorNumero(numero));

            return cartaoObjeto;
        }
        throw new RuntimeException("O cartão não foi encontrado");
    }

    public Cartao desativarCartao(String numero){
        if(cartaoRepository.existsByNumero(numero)){
            buscarCartaoPorNumero(numero).setAtivo(false);
            Cartao cartaoObjeto = cartaoRepository.save(buscarCartaoPorNumero(numero));

            return cartaoObjeto;
        }
        throw new RuntimeException("O cartão não foi encontrado");
    }



}
