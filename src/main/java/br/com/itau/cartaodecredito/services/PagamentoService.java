package br.com.itau.cartaodecredito.services;

import br.com.itau.cartaodecredito.models.Cartao;
import br.com.itau.cartaodecredito.models.Pagamento;
import br.com.itau.cartaodecredito.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PagamentoService {

    @Autowired
    PagamentoRepository pagamentoRepository;

    @Autowired
    CartaoService cartaoService;

    public Pagamento incluirPagamento(Pagamento pagamento){
        Pagamento novoPagamento = pagamentoRepository.save(pagamento);
        return novoPagamento;
    }

    public List<Pagamento> buscarPagamentoPorCartao(int id_cartao){
        Cartao cartao = cartaoService.buscarCartaoPorID(id_cartao);
        List<Pagamento> listaPagamentos = pagamentoRepository.findAllByCartao(cartao);
        if(!listaPagamentos.isEmpty()){
            return listaPagamentos;
        } throw new RuntimeException("Pagamento não encontrado");
    }
}
