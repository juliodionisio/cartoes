package br.com.itau.cartaodecredito.controllers;


import br.com.itau.cartaodecredito.models.Cliente;
import br.com.itau.cartaodecredito.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/clientes")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @PostMapping
    public ResponseEntity<Cliente> registrarCliente(@RequestBody @Valid Cliente cliente){
        Cliente clienteObjeto = clienteService.cadastrarCliente(cliente);
        return ResponseEntity.status(201).body(clienteObjeto);
    }


    @GetMapping("/{id}")
    public Cliente buscarPorID(@PathVariable(name = "id") int id){
        try{
            Cliente cliente = clienteService.buscarClientePorID(id);
            return cliente;
        } catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());

        }
    }

}
