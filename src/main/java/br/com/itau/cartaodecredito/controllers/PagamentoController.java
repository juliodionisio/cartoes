package br.com.itau.cartaodecredito.controllers;

import br.com.itau.cartaodecredito.models.Cartao;
import br.com.itau.cartaodecredito.models.Cliente;
import br.com.itau.cartaodecredito.models.Pagamento;
import br.com.itau.cartaodecredito.models.dtos.CartaoDTO;
import br.com.itau.cartaodecredito.models.dtos.PagamentoDTO;
import br.com.itau.cartaodecredito.services.CartaoService;
import br.com.itau.cartaodecredito.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/pagamentos")
public class PagamentoController {

    @Autowired
    private PagamentoService pagamentoService;

    @Autowired
    private CartaoService cartaoService;

    @PostMapping
    public ResponseEntity<PagamentoDTO> registrarPagamento(@RequestBody @Valid PagamentoDTO pagamentoDTO){

        Cartao cartao = new Cartao();
        try{
            cartao = cartaoService.buscarCartaoPorID(pagamentoDTO.getCartao_id());
        } catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }

        Pagamento pagamentoObjeto = pagamentoService.incluirPagamento(pagamentoDTO.converteParaPagamentos(cartao));
        return ResponseEntity.status(201).body(pagamentoObjeto.converteParaDTO());
    }


    @GetMapping("/{id_cartao}")
    public List<PagamentoDTO> buscarPorCartao(@PathVariable(name = "id_cartao") int id_cartao){
        try{
            Cartao cartao = cartaoService.buscarCartaoPorID(id_cartao);
            List<Pagamento> listaPagamentos = pagamentoService.buscarPagamentoPorCartao(cartao.converteParaDTO().getId());
            List<PagamentoDTO> listaPagamentosDTO = new ArrayList<>();
            for (Pagamento pagamento: listaPagamentos) {
                listaPagamentosDTO.add(pagamento.converteParaDTO());
            }
            return listaPagamentosDTO;
        } catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }
}
