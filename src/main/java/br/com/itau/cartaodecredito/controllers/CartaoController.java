package br.com.itau.cartaodecredito.controllers;


import br.com.itau.cartaodecredito.models.Cartao;
import br.com.itau.cartaodecredito.models.Cliente;
import br.com.itau.cartaodecredito.models.dtos.CartaoDTO;
import br.com.itau.cartaodecredito.services.CartaoService;
import br.com.itau.cartaodecredito.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/cartoes")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @Autowired
    private ClienteService clienteService;

    @PostMapping
    public ResponseEntity<CartaoDTO> registrarCartao(@RequestBody @Valid CartaoDTO cartaoDTO){

        Cliente cliente = new Cliente();
        try{
            cliente = clienteService.buscarClientePorID(cartaoDTO.getIdCliente());
        } catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }

        Cartao cartaoObjeto = cartaoService.cadastrarCartao(cartaoDTO.converterParaCartao(cliente));
        return ResponseEntity.status(201).body(cartaoObjeto.converteParaDTO());
    }

    @PatchMapping("/{numero}")
    public ResponseEntity<CartaoDTO> atualizarCartao(@PathVariable(name = "numero") String numero,
                                                     @RequestBody CartaoDTO cartaoDTO){

        try{
            Cartao cartaoObjeto = new Cartao();
            if(cartaoDTO.isAtivo()){
                cartaoObjeto = cartaoService.ativarCartao(numero);
            } else {
                cartaoObjeto = cartaoService.desativarCartao(numero);
            }
            return ResponseEntity.status(204).body(cartaoObjeto.converteParaDTO());
        } catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @GetMapping("/{numero}")
    public CartaoDTO buscarPorNumero(@PathVariable(name = "numero") String numero){
        try{
            Cartao cartao = cartaoService.buscarCartaoPorNumero(numero);
            return cartao.converteParaDTO();
        } catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }


}
